(* Minimal ephemeron interface to have fmt compiling (not necessary working;) *)

module K1 = struct
  let create () = Array.make 1 (None, None)

  let get_key e = let (k, _d) = e.(0) in k
  let set_key e k = let (_k, d) = e.(0) in let v = (Some k, d) in Array.set e 0 v

  let get_data e =  let (_k, d) = e.(0) in d
  let set_data e d = let (k, _d) = e.(0) in let v = (k, Some d) in Array.set e 0 v
end
